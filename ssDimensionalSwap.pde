
class DimensionalSwap extends Slitscanner {

  void createBuffer() {
    if (direction == 'x') { 
      createBufferWHM(inputFrames, inputHeight, inputWidth);
    } else if (direction == 'y') { 
      createBufferWHM(inputWidth, inputFrames, inputHeight);
    }
  }

  void determineOutput() {
    batchSize = outputBuffer.size();
    framesToRead = readLastFrame = inputFrames;
    if (direction == 'x') { outputFrames = inputWidth; }
    else if (direction == 'y') { outputFrames = inputHeight; }
    totalBatchCount = ceil((float) outputFrames / batchSize);
  }

  void nextBatch() {
    batchCount++;
    inputFrame = 0;
  }

  void processFrame() {
    if (direction == 'x') {
      int maxX = min(saveCount+batchSize, inputWidth);
      for (int x=saveCount; x<maxX; x++) {
        int target = x - saveCount;
        for (int y=0; y<inputHeight; y++) {
          int getLoc = x + y * inputWidth;
          int setLoc = inputFrame + y * inputFrames;
          outputBuffer.get(target).pixels[setLoc] = currentImage.pixels[getLoc];
        }
      }
    } else if (direction == 'y') {
      int maxY = min(saveCount+batchSize, inputHeight);
      for (int y=saveCount; y<maxY; y++) {
        int target = y - saveCount;
        for (int x=0; x<inputWidth; x++) {
          int getLoc = x + y * inputWidth;
          int setLoc = x + inputFrame * inputWidth;
          outputBuffer.get(target).pixels[setLoc] = currentImage.pixels[getLoc];
        }
      }
    }
    inputFrame++;
  }
}

