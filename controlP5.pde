
import controlP5.*;
ControlP5 cp5;

String currentInputDirectory, currentOutputFormat = "jpg", currentOutputDirectory;
int currentTab, currentDirection;
Button buttonInputDirectory, buttonOutputDirectory, buttonRender, buttonStop;
RadioButton radioDirectionDimensionalSwap, radioDirectionTimeOffset;
RadioButton radioOutputFormat;
Toggle toggleReverseOrder, togglePause, firstImage;
Textarea textAreaInfo;
String completed;
int borderLineX = 260;
int borderLineY = 270;

void setupControlP5() {
  cp5 = new ControlP5(this);

  buttonInputDirectory = cp5.addButton("selectInputDirectory").setPosition(borderLineX, 20).setSize(600, 20).setLabel("Click here to select an input directory").moveTo("global");

  cp5.getTab("default").setHeight(20).setLabel("DimensionalSwap").activateEvent(true).setId(0);
  cp5.addTab("TimeOffset").setHeight(20).activateEvent(true).setId(1);
  cp5.getWindow().setPositionOfTabs(borderLineX, 60);

  radioDirectionDimensionalSwap = cp5.addRadioButton("radioButtonDimensionalSwap").setPosition(borderLineX, 100).setSize(60, 20).setItemsPerRow(4).setSpacingColumn(10)
    .addItem("horizontal", 0).addItem("vertical", 1).setNoneSelectedAllowed(false).activate(0);
  radioDirectionDimensionalSwap.getItem(0).getCaptionLabel().getStyle().movePadding(0, 0, 0, -55);
  radioDirectionDimensionalSwap.getItem(1).getCaptionLabel().getStyle().movePadding(0, 0, 0, -51);

  radioDirectionTimeOffset = cp5.addRadioButton("radioButtonTimeOffset").setPosition(borderLineX, 100).setSize(40, 20).setItemsPerRow(4).setSpacingColumn(10)
    .addItem("right", 0).addItem("left", 1).addItem("up", 2).addItem("down", 3).setNoneSelectedAllowed(false).activate(0).moveTo("TimeOffset");
  radioDirectionTimeOffset.getItem(0).getCaptionLabel().getStyle().movePadding(0, 0, 0, -33);
  radioDirectionTimeOffset.getItem(1).getCaptionLabel().getStyle().movePadding(0, 0, 0, -32);
  radioDirectionTimeOffset.getItem(2).getCaptionLabel().getStyle().movePadding(0, 0, 0, -29);
  radioDirectionTimeOffset.getItem(3).getCaptionLabel().getStyle().movePadding(0, 0, 0, -35);

  toggleReverseOrder = cp5.addToggle("reverseFileOrder").setPosition(borderLineX, 140).setSize(90, 20).moveTo("global");
  toggleReverseOrder.getCaptionLabel().setPadding(6, -13);

  firstImage = cp5.addToggle("firstImage").setPosition(borderLineX+100, 140).setSize(90, 20).moveTo("TimeOffset");
  firstImage.getCaptionLabel().setPadding(8, -13);

  radioOutputFormat = cp5.addRadioButton("radioButtonOutput").setPosition(borderLineX, 180).setSize(40, 20).setItemsPerRow(4).setSpacingColumn(10)
    .addItem("jpg", 0).addItem("png", 1).addItem("tga", 2).setNoneSelectedAllowed(false).activate(0).moveTo("global");
  radioOutputFormat.getItem(0).getCaptionLabel().getStyle().movePadding(0, 0, 0, -32);
  radioOutputFormat.getItem(1).getCaptionLabel().getStyle().movePadding(0, 0, 0, -31);
  radioOutputFormat.getItem(2).getCaptionLabel().getStyle().movePadding(0, 0, 0, -30);

  textAreaInfo = cp5.addTextarea("info").setPosition(borderLineX+200, 60).setLineHeight(12).setSize(400, 48).setColorBackground(color(0xff003652))
    .setText("\n This text area will display information about:\n input, output, memory & batchCount").moveTo("global");

  buttonRender = cp5.addButton("render").setPosition(borderLineX+200, 121).setSize(400, 48).setLabel("Click here to start rendering").moveTo("global");
  buttonRender.getCaptionLabel().setPaddingX(127);
  buttonStop = cp5.addButton("reset").setPosition(borderLineX+200, 179).setSize(40, 20).moveTo("global");
  buttonStop.getCaptionLabel().setPaddingX(8);

  togglePause = cp5.addToggle("pause").setPosition(borderLineX+250, 179).setSize(40, 20).moveTo("global");
  togglePause.getCaptionLabel().setPadding(8, -13);

  buttonOutputDirectory = cp5.addButton("selectOutputDirectory").setPosition(borderLineX, 219).setSize(600, 20).setLabel("Click here to select an output directory").moveTo("global");
}

void selectInputDirectory() {
  selectFolder("select input folder","folderSelectedInput");
  
}

void selectOutputDirectory() {
  selectFolder("select output folder","folderSelectedOutput");
}

void folderSelectedOutput(File selectedDirectory) {
  if (selectedDirectory != null) {
    currentOutputDirectory = selectedDirectory.getAbsolutePath();
    buttonOutputDirectory.setLabel(selectedDirectory.getAbsolutePath());
  }
}
void folderSelectedInput(File selectedDirectory) {
  if (selectedDirectory != null) {
      currentInputDirectory = selectedDirectory.getAbsolutePath();
      buttonInputDirectory.setLabel(selectedDirectory.getAbsolutePath());
    }
}

void render() {
  if (currentInputDirectory == null) {
    buttonRender.setLabel("Select an input directory first!");
  } else {
    togglePause.setState(false);
    buttonRender.setLabel("          Rendering!");
    if (slitsP5 != null) { 
      slitsP5 = null; 
      System.gc();
    }
    if (currentTab == 0) { 
      slitsP5 = new DimensionalSwap();
    } else { 
      slitsP5 = new TimeOffset();
    }
  }
}

void reset() {
  completed = "";
  buttonRender.setLabel("Click here to start rendering");
  textAreaInfo.setText("\n This text area will display information about:\n input, output, memory & batchCount");
  togglePause.setState(false);
  slitsP5 = null; 
  System.gc();
}

void controlEvent(ControlEvent theEvent) {
  if (theEvent.isTab()) {
    currentTab = theEvent.getTab().getId();
  } else if (theEvent.isFrom(radioDirectionDimensionalSwap) || theEvent.isFrom(radioDirectionTimeOffset)) {
    currentDirection = (int) theEvent.getValue();
  } else if (theEvent.isFrom(radioOutputFormat)) {
    currentOutputFormat = radioOutputFormat.getItem((int)theEvent.getValue()).getName();
  }
}

