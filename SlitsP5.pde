import java.applet.*;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.MouseEvent;
import java.awt.event.KeyEvent;
import java.awt.event.FocusEvent;
import java.awt.Image;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import java.util.zip.*;
import java.util.regex.*;
// SlitsP5 v0.5
// by Amnon Owed @ http://amnonp5.wordpress.com (12-06-2012)
// Tested under Processing 1.5.1 & ControlP5 0.7.5
// 2013-06-02 - fixed by StudioEtrange/nomorgan : support for processing 2.0b9 revision 216 and ControlP5 2.0.4 

Slitscanner slitsP5;

void setup() {
  size(1280, 720);
  setupControlP5();
  textFont(createFont("Arial", 18));
  smooth();
}

void draw() {
  background(0xffE0E4CC);
  if (slitsP5 == null) { 
    displayExplanation();
  } else {
    slitsP5.update();
  }
  displayControls();
}

