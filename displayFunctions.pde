
void displayExplanation() {
  textAlign(LEFT, TOP);
  text("\nShort explanation:\n1. Select an input directory\n2. Choose the custom settings for mode, direction, order, output format & output directory\n3. Click the render button\n\nTIP: Increase the maximum available memory in Processing via File > Preferences", 10, borderLineY);
}

String[] controlStrings = { "INPUT DIRECTORY", "MODE", "DIRECTION", "ORDER", "OUTPUT FORMAT", "OUTPUT DIRECTORY" };

void displayControls() {
  noStroke();
  fill(0xffF38630);
  rect(0, 0, width, borderLineY-10);

  int diameter = 30;
  for (int i=0; i<controlStrings.length; i++) {
    fill(255);
    ellipse(30, 30+i*40, diameter, diameter);
    fill(0);
    textAlign(CENTER, CENTER);
    text(i+1, 30, 30+i*40-2);
    textAlign(LEFT, CENTER);
    text(controlStrings[i], 53, 30+i*40-2);
  }

  textAlign(LEFT, TOP);
  if (completed != null) { text(completed, borderLineY+290, 179); }
}

