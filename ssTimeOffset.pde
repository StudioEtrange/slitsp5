
class TimeOffset extends Slitscanner {

  void createBuffer() {
    createBufferWHM(inputWidth, inputHeight, inputFrames);
  }

  void determineOutput() {
    framesToRead = readLastFrame = batchSize = outputBuffer.size();
    outputFrames = inputFrames;
    totalBatchCount = ceil((float) outputFrames / batchSize);
  }

  void nextBatch() {
    batchCount++;
    readLastFrame += batchSize;
    if (readLastFrame > inputFrames) {
      readLastFrame = inputFrames;
    }
    inputFrame = readFirstFrame = max(0, readLastFrame-inputHeight-batchSize);
    framesToRead = readLastFrame - readFirstFrame;
  }

  void processFrame() {
    if (direction == 'x') {
      int minX = max(0, saveCount-inputFrame);
      int maxX = min(readLastFrame-inputFrame, inputWidth);
      for (int x=minX; x<maxX; x++) {
        int target = x + inputFrame - saveCount;
        for (int y=0; y<inputHeight; y++) {
          int index;
          if (invertEffect) { index = (inputWidth-1-x) + y * inputWidth; }
          else { index = x + y * inputWidth; }
          outputBuffer.get(target).pixels[index] = currentImage.pixels[index];
        }
      }
    } else if (direction == 'y') {
      int minY = max(0, saveCount-inputFrame);
      int maxY = min(readLastFrame-inputFrame, inputHeight);
      for (int y=minY; y<maxY; y++) {
        int target = y + inputFrame - saveCount;
        for (int x=0; x<inputWidth; x++) {
          int index;
          if (invertEffect) { index = x + (inputHeight-1-y) * inputWidth; }
          else { index = x + y * inputWidth; }
          outputBuffer.get(target).pixels[index] = currentImage.pixels[index];
        }
      }
    }
    inputFrame++;
  }
}

