# SlitP5 - Slit Scanning technic in Processing

by Amnon Owed ([http://amnonp5.wordpress.com](http://amnonp5.wordpress.com))

This repository is a mirror of the [original repository](https://code.google.com/p/amnonp5/) adding some fixes.


### About

SlitP5 :

* [https://code.google.com/p/amnonp5/](https://code.google.com/p/amnonp5/)

Slit scanning :

* [http://amnonp5.wordpress.com/2011/01/16/eternalism-the-art-of-slitscanning/](http://amnonp5.wordpress.com/2011/01/16/eternalism-the-art-of-slitscanning/)



### Changelog

#### v0.4.1

changes and fixes by StudioEtrange/nomorgan ([http://www.studio-etrange.net](http://www.studio-etrange.net))

* Adapted HemeshGui to run under the latest Processing version (2.0)
* Adapted HemeshGui to run under the latest ControlP5 (2.0.4)


### Installation
