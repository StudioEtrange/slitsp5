
abstract class Slitscanner {

  // ============= GENERAL VARIABLES ============= //

  ArrayList <PImage> outputBuffer = new ArrayList <PImage> ();
  int inputWidth, inputHeight, inputFrames, outputFrames, inputFrame;
  int batchSize, totalBatchCount, batchCount, saveCount;
  int readFirstFrame, readLastFrame, framesToRead;
  int maxMemory, totalMemory, freeMemory;
  String[] loadFilenames;
  PImage currentImage;
  String outputName, outputDir;
  boolean done;
  int displayX, displayY, displayHeight, displayWidth;
  float barWidth, barHeight;
  int startTime;

  // ============= SPECIAL VARIABLES ============= //
  
  int memReserved = 100;
  String fullPath, outputFormat;
  boolean reverseOrder, invertEffect;
  char direction;

  // ============= CONSTRUCTOR ============= //

  Slitscanner() {
    fullPath = currentInputDirectory;
    outputFormat = currentOutputFormat;
    reverseOrder = toggleReverseOrder.getState();
    if (currentTab == 0) {
      if (currentDirection == 0) { direction = 'x'; } else { direction = 'y'; }
    } else {
      if (currentDirection <= 1) { direction = 'x'; } else { direction = 'y'; }
      if (currentDirection == 1 || currentDirection == 2) { invertEffect = true; }
    }
    setupSlitscanner();
  }

  // ============= SUBTYPE FUNCTIONS ============= //

  abstract void createBuffer();
  abstract void determineOutput();
  abstract void processFrame();
  abstract void nextBatch();

  // ============= SETUP FUNCTIONS ============= //

  void setupSlitscanner() {
    loadFilenames();
    if (loadFilenames.length > 0) {
      determineInput();
      createBuffer();
      determineOutput();
      createOutputName();
      createOutputDir();
      setTextareaInfo();
      setDisplaySettings();
      setStartSettings();
    }
  }

  void loadFilenames() {
    java.io.File folder = new java.io.File(fullPath);
    java.io.FilenameFilter imgFilter = new java.io.FilenameFilter() {
      public boolean accept(File dir, String name) {
        String lowcase = name.toLowerCase();
        return lowcase.endsWith(".jpg")
        || lowcase.endsWith(".jpeg")
        || lowcase.endsWith(".png")
        || lowcase.endsWith(".tga");
      }
    };
    loadFilenames = folder.list(imgFilter);
    if (reverseOrder) { Arrays.sort(loadFilenames, Collections.reverseOrder()); }
  }

  void determineInput() {
    PImage tempImage = loadImageFast(fullPath + "/" + loadFilenames[0]);
    inputWidth = tempImage.width;
    inputHeight = tempImage.height;
    inputFrames = loadFilenames.length;
  }

  void createBufferWHM(int w, int h, int maxBuffer) {
    PImage tempImage;
    if (currentTab == 1 && firstImage.getState()) {
      tempImage = loadImageFast(fullPath + "/" + loadFilenames[0]);
    } else {
      tempImage = createImage(w, h, RGB);
    }
    maxMemory = int(Runtime.getRuntime().maxMemory()/1000000);
    for (int i=0; i<maxBuffer; i++) {
      outputBuffer.add(tempImage.get());
      totalMemory = int(Runtime.getRuntime().totalMemory()/1000000);
      if (maxMemory == totalMemory) {
        freeMemory = int(Runtime.getRuntime().freeMemory()/1000000);
        if (freeMemory < memReserved) {
          break;
        }
      }
    }
    System.gc();
  }

  void createOutputName() {
    String timestamp = year() + nf(month(), 2) + nf(day(), 2) + "-"  + nf(hour(), 2) + nf(minute(), 2) + nf(second(), 2);
    String settings = "";
    if (currentTab == 0) {
      settings += "-ds";
      if (direction == 'x') { settings += "-h"; }
      else { settings += "-v"; }
    } else {
      settings += "-to";
      if (currentDirection == 0) { settings += "-right"; }
      if (currentDirection == 1) { settings += "-left"; }
      if (currentDirection == 2) { settings += "-up"; }
      if (currentDirection == 3) { settings += "-down"; }
    }
    outputName = timestamp + settings;
  }

  void createOutputDir() {
    if (currentOutputDirectory == null) {
      outputDir = "output";
    } else {
      outputDir = currentOutputDirectory;
    }
  }
  
  void setTextareaInfo() {
    textAreaInfo.setText(
       "IN: inputWidth = " + inputWidth + " | inputHeight = " + inputHeight + " | inputFrames = " + inputFrames + "\n" +
       "OUT: outputWidth = " + outputBuffer.get(0).width + " | outputHeight = " + outputBuffer.get(0).height + " | outputFrames = " + outputFrames + "\n" +
       "MEM: " + maxMemory + " (max) | " + totalMemory + " (total) | " + memReserved + " (reserved) | " + freeMemory + " (free)" + "\n" +
       "BATCH: batchSize = " + batchSize  + " | batchCount = " + totalBatchCount
    );
  }

  void setDisplaySettings() {
    displayHeight = height-borderLineY-10;
    displayWidth = int(width * (float) displayHeight/height);
    displayX = width-displayWidth-10;
    displayY = borderLineY;
    barWidth = displayX-20;
    barHeight = (displayHeight-25-2*(totalBatchCount-1))/totalBatchCount;
  }
  
  void setStartSettings() {
    startTime = millis();
    completed = "";
  }

  // ============= UPDATE FUNCTIONS ============= //

  void update() {
    if (done) {
      saveFrames();
      nextBatch();
      done = false;
    }

    if (!togglePause.getState()) {
      if (batchCount < totalBatchCount) {
        currentImage = loadImageFast(fullPath + "/" + loadFilenames[inputFrame]);
        image(currentImage, displayX, displayY, displayWidth, displayHeight);
        processFrame();
        drawProgress();
      } else {
        end();
      }
    } else {
      if (currentImage != null) {
        image(currentImage, displayX, displayY, displayWidth, displayHeight);
      }
      drawProgress();
    }

    if (inputFrame == readLastFrame) {
      done = true;
      drawSaving();
    }
  }

  void drawProgress() {
    float percentageBatch = (float)(inputFrame-readFirstFrame)/framesToRead;
    float oneBatch = 1.0/totalBatchCount;
    float percentageTotal = (batchCount+percentageBatch)*oneBatch;
    pushMatrix();
    translate(10, displayY+5);

    fill(0);
    text("PROGRESS: " + round(percentageBatch*100) + "% for batch " + (batchCount+1) + " of " + totalBatchCount + " (" + round(percentageTotal*100) +"%)", 0, 0);

    translate(0, 20);
    for (int i=0; i<totalBatchCount; i++) {
      if (i < batchCount) { 
        fill(0xff69D2E7);
      } else { 
        fill(0xff003652);
      }
      rect(0, i*(barHeight+2), barWidth, barHeight);
    }
    fill(0xff69D2E7);
    rect(0, batchCount*(barHeight+2), barWidth * percentageBatch, barHeight);
    popMatrix();
  }

  void drawSaving() {
    fill(255, 125);
    rect(displayX, displayY, displayWidth, displayHeight);
    fill(0);
    text("SAVING!", displayX+displayWidth/2-10, displayY+displayHeight/2);
  }

  void saveFrames() {
    for (PImage p : outputBuffer) {
      if (saveCount < outputFrames) {
        p.save(outputDir + "/" + outputName + "/sequence_" + nf(saveCount, 4) + "." + outputFormat);
        saveCount++;
      } else {
        break;
      }
    }
  }

  void end() {
    if (loadFilenames.length > 0) {
      int elapsedTime = millis() - startTime;
      int hours = elapsedTime / (1000*60*60);
      int minutes = (elapsedTime % (1000*60*60)) / (1000*60);
      int seconds = ((elapsedTime % (1000*60*60)) % (1000*60)) / 1000;
      completed = "Rendering completed in " + hours + " hours " + minutes + " minutes and " + seconds + " seconds!";
      buttonRender.setLabel("Click here to start rendering");
      textAreaInfo.setText("\n This text area will display information about:\n input, output, memory & batchCount");
    } else {
      completed = "No image files (jpg, png, tga) in specified directory!";
    }
    slitsP5 = null;
    System.gc();
  }

  PImage loadImageFast(String inFile) {
    if (inFile.toLowerCase().endsWith(".jpg") || inFile.toLowerCase().endsWith(".jpeg") ||  inFile.toLowerCase().endsWith(".png")) {
      byte[] bytes = loadBytes(inFile);
      if (bytes != null) {
        Image image = java.awt.Toolkit.getDefaultToolkit().createImage(bytes);
        int [] data= new int [1];
        PImage pi = null;
        try {
          java.awt.image.PixelGrabber grabber = new java.awt.image.PixelGrabber(image, 0, 0, -1, -1, false);
          if (grabber.grabPixels()) {
            int w = grabber.getWidth();
            int h = grabber.getHeight();
            pi = createImage(w, h, RGB);
            arraycopy(grabber.getPixels(), pi.pixels);
          }
        }
        catch (InterruptedException e) {
          System.err.println("Problems! Defaulting to loadImage(). Error: " + e);
          return loadImage(inFile);
        }
        if (pi != null) {
          return pi;
        }
        return loadImage(inFile);
      }
      return loadImage(inFile);
    }
    return loadImage(inFile);
  }
}

